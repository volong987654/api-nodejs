const User = require('../models/User') 


const index = async (req, res) => {
    try {
        const users = await User.find({});
        console.log("Users:", users);

        return res.status(200).json({
            message: 'Users retrieved successfully.',
            users: users
        });
    } catch (error) {
        console.error(error);
        return res.status(500).json({ error: 'An error occurred while fetching users.' });
    }
};

const newUser = async (req, res, next) => {

    // console.log('req.body content', req.body);
    // const newUser = new User(req.body);
    // console.log('req.body content', newUser);
    try {
        console.log('req.body content', req.body);

        const userToCreate = new User(req.body);
        const createdUser = await userToCreate.save();

       //const user = await User.create(req.body);
       

        // newUser.save((err, user) => {
        //     console.log('req.body content', err);
        //     console.log('req.body content', user);
        // })
        // Do something with req.body

        // If you're using 'next', call it to pass control to the next middleware/route handler
        // next();
        
        return res.status(200).json({
            message: 'User created successfully.',
           // user: user
        });
    } catch (error) {
        console.error('Error:', error);
        return res.status(500).json({
            error: 'An error occurred while creating a new user.'
        });
    }
};

module.exports = {
    index,
    newUser
}