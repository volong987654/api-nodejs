const http = require('http')

const COURES = [
    {id: 1, name: 'NodeJS'},
    {id: 1, name: 'REACTJS'}
]

const server = http.createServer((req, res) =>{
res.setHeader('Content-type', 'application/json')
res.end(JSON.stringify({
    success: true,
    data: COURES
}) );
})

const PORT = 3000;
server.listen(PORT, () => console.log(`Server is listening on port ${PORT}`))